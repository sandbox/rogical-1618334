(function ($) {
  Drupal.behaviors.geofieldAliMap = {
    attach: function(context, settings) {
      
      $('.geofieldAliMap', context).once('geofield-processed', function(index, element) {
        var data = undefined;
        var map_settings = [];
        var address = undefined;
        var pointCount = 0;
        var resetZoom = true;
        var elemID = $(element).attr('id');

        if(settings.geofieldAliMap[elemID]) {
            data = settings.geofieldAliMap[elemID].data;
            map_settings = settings.geofieldAliMap[elemID].map_settings;//alert(data['coordinates'][1]);
            address = settings.geofieldAliMap[elemID].address;
        }
        //alert('22');Drupal.settings.geofieldAliMap['geofield-alimap-entity-node-5-field-geo2']['data']['coordinates']
        //alert(data.lat);
        map=new AliMap(elemID); //使用id为mapDiv的层创建一个地图对象
        //map.centerAndZoom(new AliLatLng(30.238747,120.14532),15);//显示地图
        map.centerAndZoom(new AliLatLng(data.coordinates[1], data.coordinates[0]), map_settings.zoom);//显示地图
        //创建一个导航控件并添加到地图
        control=new AliMapLargeControl()
        map.addControl(control);
        //给地图添加滚轮缩放功能
        control=new AliMapMouseWheelControl();
        map.addControl(control);
        //定义一个经纬度
        var latlng=new AliLatLng(data.coordinates[1],data.coordinates[0]);
        //在该坐标处创建一个标记
        marker=new AliMarker(latlng);
        //将该标记添加到地图
        map.addOverlay(marker);
        marker._title = address.thoroughfare;
        marker._desc = address.premise;//将标记的信息浮窗内容记录下来
        AliEvent.bind(marker, "mouseover" , marker,onMarkerMouseover );    
        
        // Checking to see if google variable exists. We need this b/c views breaks this sometimes. Probably
        // an AJAX/external javascript bug in core or something.
        if (typeof google != 'undefined' && typeof google.maps.ZoomControlStyle != 'undefined' && data != undefined) {
          var features = GeoJSON(data);
          // controltype
          var controltype = map_settings.controltype;
          if (controltype == 'default') { controltype = google.maps.ZoomControlStyle.DEFAULT; }
          else if (controltype == 'small') { controltype = google.maps.ZoomControlStyle.SMALL; }
          else if (controltype == 'large') { controltype = google.maps.ZoomControlStyle.LARGE; }
          else { controltype = false }

          // map type
          var maptype = map_settings.maptype;
          if (maptype) {
            if (maptype == 'map' && map_settings.baselayers_map) { maptype = google.maps.MapTypeId.ROADMAP; }
            if (maptype == 'satellite' && map_settings.baselayers_satellite) { maptype = google.maps.MapTypeId.SATELLITE; }
            if (maptype == 'hybrid' && map_settings.baselayers_hybrid) { maptype = google.maps.MapTypeId.HYBRID; }
            if (maptype == 'physical' && map_settings.baselayers_physical) { maptype = google.maps.MapTypeId.TERRAIN; }
          }
          else { maptype = google.maps.MapTypeId.ROADMAP; }

          // menu type
          var mtc = map_settings.mtc;
          if (mtc == 'standard') { mtc = google.maps.MapTypeControlStyle.HORIZONTAL_BAR; }
          else if (mtc == 'menu' ) { mtc = google.maps.MapTypeControlStyle.DROPDOWN_MENU; }
          else { mtc = false; }

          var myOptions = {
            zoom: parseInt(map_settings.zoom),
            mapTypeId: maptype,
            mapTypeControl: (mtc ? true : false),
            mapTypeControlOptions: {style: mtc},
            zoomControl: ((controltype !== false) ? true : false),
            zoomControlOptions: {style: controltype},
            panControl: (map_settings.pancontrol ? true : false),
            scrollwheel: (map_settings.scrollwheel ? true : false),
            draggable: (map_settings.draggable ? true : false),
            overviewMapControl: (map_settings.overview ? true : false),
            overviewMapControlOptions: {opened: (map_settings.overview_opened ? true : false)},
            streetViewControl: (map_settings.streetview_show ? true : false),
            scaleControl: (map_settings.scale ? true : false),
            scaleControlOptions: {style: google.maps.ScaleControlStyle.DEFAULT}
          };

          var map = new google.maps.Map($(element).get(0), myOptions);
          var range = new google.maps.LatLngBounds();

          var infowindow = new google.maps.InfoWindow({
            content: ''
          });

          if (features.setMap) {
            placeFeature(features, map, range);
            // Don't move the default zoom if we're only displaying one point.
            if (features.getPosition) {
              resetZoom = false;
            }
          } else {
            for (var i in features) {
              if (features[i].setMap) {
                placeFeature(features[i], map, range);
              } else {
                for (var j in features[i]) {
                  if (features[i][j].setMap) {
                    placeFeature(features[i][j], map, range);
                  }
                }
              }
            }
          }

          if (resetZoom) {
            map.fitBounds(range);
          } else {
            map.setCenter(range.getCenter());
          }
        }
        
        function placeFeature(feature, map, range) {
          var properties = feature.get('geojsonProperties');
          if (feature.setTitle && properties && properties.title) {
            feature.setTitle(properties.title);
          }
          feature.setMap(map);
          if (feature.getPosition) {
            range.extend(feature.getPosition());
          } else {
            var path = feature.getPath();
            path.forEach(function(element) {
              range.extend(element);
            });
          }

          if (properties && properties.description) {
            var bounds = feature.get('bounds');
            google.maps.event.addListener(feature, 'click', function() {
              infowindow.setPosition(bounds.getCenter());
              infowindow.setContent(properties.description);
              infowindow.open(map);
            });
          }
        }
      });
      
      //在每个标记点击的时候显示自身的信息浮窗内容
      function onMarkerMouseover() {
        var infowin=this.openInfoWindow(this._title,this._desc);
        //确保信息浮窗在视图范围内
        infowin.moveIntoView();
      }
      
    }
  }
})(jQuery);
