<?php

/**
 * @file
 * A specific handler for CH.
 */

$plugin = array(
  'title' => t('Address form (CN add-on from China Address Field)'),
  'format callback' => 'addressfield_format_address_cn_generate',
  'type' => 'address',
  'weight' => -80,
);//dpm('11');

function addressfield_format_address_cn_generate(&$format, $address, $context = array()) {//dpm($context);
  if ($address['country'] == 'CN' && $context['mode'] == 'form') {
    //dpm($address);
    // Set default value
    $province = isset($address['administrative_area']) ? $address['administrative_area'] : 2;
    $city = isset($address['city']) ? $address['city'] : 52;
    $county = isset($address['county']) ? $address['county'] : 500;
    
    $cities = china_address_cities($province);//dpm($province);
    $city_keys = array_keys($cities);
    if (!in_array($city, $city_keys)) {
      $city = $city_keys[0];
    }
    
    $counties = china_address_counties($city);
    $county_keys = array_keys($counties);
    if (!in_array($county, $county_keys)) {
      $county = $county_keys[0];
    }
    
    $format['locality_block'] = array(
        '#type' => 'addressfield_container',
        '#attributes' => array('class' => array('addressfield-container-inline', 'locality-block', 'country-' . $address['country'])),
        '#weight' => 0,
    );
    $format['locality_block']['administrative_area'] = array(
      '#type' => 'select',
      '#title' => t('Province'),
      '#options' => china_address_provinces(),
      '#prefix' => '<div class="china-address province">',
      '#suffix' => '</div>',
      //'#attributes' => array('class' => array('china-address province')),
      '#attached' => array(
        'css' => array(
          array(
             'data' => '.china-address .form-item {float:left;margin-right:10px;}',
             'type' => 'inline',
          ),
        ),
      ),
    );
 /*   
    $format['street_block']['province'] = array(
          '#type' => 'select',
          '#title' => t('Province'),
          '#default_value' => $province,
          '#options' => china_address_provinces(),
          '#weight' => -3,
          '#prefix' => '<div class="china-address province">',
          '#attached' => array(
            'css' => array(
    array(
                'data' => '.china-address .form-item { float: left; margin-right: 10px;}',
                'type' => 'inline',
    ),
    ),
    ),
    );*/
    
    $format['locality_block']['locality'] = array(
      '#type' => 'select',
      '#title' => t('City'),
      '#default_value' => $city,
      '#options' => $cities,
      '#prefix' => '<div class="china-address city">',
      '#suffix' => '</div>',
    );
    
    /*
    $format['street_block']['city'] = array(
          '#type' => 'select',
          '#title' => t('City'),
          '#default_value' => $city,
          '#options' => $cities,
          '#weight' => -2,
          '#prefix' => '<div class="china-address city">',
    );*/
    
    //$format['street_block'] = array(
    //    '#type' => 'addressfield_container',
    //    '#attributes' => array('class' => array('street-block')),
    //    '#weight' => 1,
    //);
    unset($format['street_block']['thoroughfare']);
    $format['locality_block']['thoroughfare'] = array(
      '#type' => 'select',
      '#title' => t('County'),
      '#options' => $counties,
      '#default_value' => $county,
      '#prefix' => '<div class="china-address county">',
      '#suffix' => '</div>',
    );
    
    
    /*
    $format['street_block']['county'] = array(
          '#type' => 'select',
          '#title' => t('County'),
          '#default_value' => $county,
          '#options' => $counties,
          '#weight' => -1,
          '#prefix' => '<div class="china-address county">',
          '#attached' => array(
            'css' => array(
    array(
                'data' => '.china-address.county .form-item { float: none;}',
                'type' => 'inline',
    ),
    ),
    ),
    );*/
    
    $format['street_block']['premise'] = array(
        '#type' => 'textfield',
        '#title' => t('Detail'),
        '#size' => 30,
    );
    
    unset($format['locality_block']['postal_code']);
    $format['street_block']['postal_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Postal Code'),
        '#size' => 10,
    );
    
    //dpm($format['street_block']['province']);
    $format['locality_block']['administrative_area']['#wrapper_id'] = $format['#wrapper_id'];
    $format['locality_block']['administrative_area']['#process'][] = 'ajax_process_form';
    $format['locality_block']['administrative_area']['#process'][] = 'addressfield_format_address_cn_province_process';
    $format['locality_block']['administrative_area']['#ajax'] = array(
          'callback' => 'addressfield_standard_widget_refresh',
          'wrapper' => $format['#wrapper_id'],
          'method' => 'replace',
    );
    
    $format['locality_block']['locality']['#wrapper_id'] = $format['#wrapper_id'];
    $format['locality_block']['locality']['#process'][] = 'ajax_process_form';
    $format['locality_block']['locality']['#process'][] = 'addressfield_format_address_cn_province_process';
    $format['locality_block']['locality']['#ajax'] = array(
              'callback' => 'addressfield_standard_widget_refresh',
              'wrapper' => $format['#wrapper_id'],
              'method' => 'replace',
    );
    
    //$format['locality_block']['postal_code']['#wrapper_id'] = $format['#wrapper_id'];
   // $format['locality_block']['postal_code']['#process'][] = 'ajax_process_form';
    //$format['locality_block']['postal_code']['#process'][] = 'addressfield_format_address_cn_postal_code_process';

    //$format['locality_block']['postal_code']['#element_validate'] = array('addressfield_form_cn_postal_code_validation');
    //$format['locality_block']['postal_code']['#ajax'] = array(
    //  'callback' => 'addressfield_standard_widget_refresh',
    //  'wrapper' => $format['#wrapper_id'],
    //  'method' => 'replace',
    //);
  }
  else {
    if (isset($format['locality_block']['postal_code'])) {
      // Cancel the AJAX for forms we don't control.
      $format['locality_block']['postal_code']['#ajax'] = array();
    }
  }
}

function addressfield_format_address_cn_postal_code_process($element) {
  $element['#limit_validation_errors'] = array($element['#parents']);

  return $element;
}

function addressfield_format_address_cn_province_process($element) {
  $element['#limit_validation_errors'] = array($element['#parents']);

  return $element;
}

function addressfield_form_cn_postal_code_validation($element, &$form_state, &$form) {
  $data = array(
    '1000' => array('town' => 'Lausanne0', 'canton' => 'VD'),
    '1001' => array('town' => 'Lausanne1', 'canton' => 'VD'),
    '1002' => array('town' => 'Lausanne2', 'canton' => 'VD'),
  );

  // Check if theres something for autocomplete
  if (!empty($element['#value']) && (isset($data[$element['#value']]))) {
    // Get the base #parents for this address form.
    $base_parents = array_slice($element['#parents'], 0, -1);
    $base_array_parents = array_slice($element['#array_parents'], 0, -2);

    $city = $data[$element['#value']];

    // Set the new values in the form.
    drupal_array_set_nested_value($form_state['values'], array_merge($base_parents, array('locality')), $city['town'], TRUE);
    drupal_array_set_nested_value($form_state['values'], array_merge($base_parents, array('administrative_area')), $city['canton'], TRUE);

    // Discard value the user has already entered there.
    drupal_array_set_nested_value($form_state['input'], array_merge($base_parents, array('locality')), NULL, TRUE);
    drupal_array_set_nested_value($form_state['input'], array_merge($base_parents, array('administrative_area')), NULL, TRUE);
  }
}