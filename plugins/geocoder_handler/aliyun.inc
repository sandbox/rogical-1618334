<?php
// $Id$

/**
 * @file
 * Plugin to provide a google geocoder.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Aliyun Geocoder"),
  'description' => t('Geocodes via aliyun geocoder'),
  'callback' => 'geocoder_aliyun',
  'field_types' => array('text', 'text_long', 'addressfield', 'location', 'text_with_summary', 'computed', 'taxonomy_term_reference'),
  'field_callback' => 'geocoder_aliyun_field',
  //'settings_callback' => 'geocoder_aliyun_form',
  'terms_of_service' => 'http://ditu.aliyun.com/jsdoc/geocode_api.html',
);

/**
 * Process Markup
 */
function geocoder_aliyun($address, $options = array()) {
  geophp_load();
  
  $query = array(
    'a' => $address,
    //'sensor' => 'false'
  );
  //dpm($address);
  //$url = url("http://maps.googleapis.com/maps/api/geocode/json", array('query' => $query));
  $url = url("http://gc.ditu.aliyun.com/geocoding", array('query' => $query));//dpm($url);
  //$url = url("http://gc.ditu.aliyun.com/geocoding?a=%E6%B5%B7%E6%B7%80%E7%9F%A5%E6%98%A5%E8%B7%AF28%E5%8F%B7");
  $result = drupal_http_request($url);//dpm($result);
  
  if (isset($result->error)) {
    drupal_set_message(t('Error geocoding') . ' - ' . $result->code . ' ' . $result->error, 'error');
    return FALSE;
  }
  
  $data = json_decode($result->data);//dpm($data);
  
  return _geocoder_aliyun_geometry($data);
}

function geocoder_aliyun_field($field, $field_item, $options = array()) {
  if ($field['type'] == 'text' || $field['type'] == 'text_long' || $field['type'] == 'text_with_summary' || $field['type'] == 'computed') {
    return geocoder_aliyun($field_item['value'], $options);
  }
  if ($field['type'] == 'addressfield') {//dpm($field_item);
    //$address = geocoder_widget_parse_addressfield($field_item);
    $address = $field_item['administrative_area'].$field_item['locality'].$field_item['thoroughfare'].$field_item['premise'];//@todo needs completely testing
    return geocoder_aliyun($address, $options);
  }
  if ($field['type'] == 'location') {
    $address = geocoder_widget_parse_locationfield($field_item);
    return geocoder_aliyun($address, $options);
  }
  if ($field['type'] == 'taxonomy_term_reference') {
    $term = taxonomy_term_load($field_item['tid']);
    return geocoder_aliyun($term->name);
  }
}

function _geocoder_aliyun_geometry(&$data) {
  if (!isset($data->lon, $data->lat)) {
    return NULL;
  }
  return new Point($data->lon, $data->lat);
}

